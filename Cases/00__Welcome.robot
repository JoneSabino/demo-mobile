*** Settings ***
Resource          ../Resources/Pages/Welcome/welcome.robot
Resource          ../Resources/env.robot
Suite Setup       Init
Suite Teardown    Close Application

*** Test Cases ***
Skip button visibility
    [tags]    TC-123
    Given the page "1" is fully loaded
    Then I Can See the Skip button

Welcome 1st page text validation
    Given the page "1" is fully loaded
    Then I have to se the text "bem-vindo ao novo \naplicativo da c&a"
    And The text needs to use capital letters properly

Welcome 2nd page text validation
    Given the page "2" is fully loaded
    Then I have to se the text "faça suas compras online \npelo app"
    And The text needs to use capital letters properly

Welcome 3rd page text validation
    Given the page "3" is fully loaded
    Then I have to se the text "não encontrou seu \ntamanho na loja?"
    And The text needs to use capital letters properly

Welcome 4th page text validation
    Given the page "4" is fully loaded
    Then I have to se the text "encontre a c&a mais \npróxima de você"
    And The text needs to use capital letters properly

Welcome 5th page text validation
    Given the page "5" is fully loaded
    Then I have to se the text "fique por dentro das \nnovidades"
    And The text needs to use capital letters properly

Welcome 6th page text validation
    Given the page "6" is fully loaded
    Then I have to se the text "ative sua localização \npara ganhar descontos"
    And The text needs to use capital letters properly

Test Skip button
    Skip Welcome Section

Validate signup/login page
    Given the access page is fully loaded
    Then I have to see the signup and signin buttons



    
