import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomLibrary {
    public boolean isUpperCase(String text) {
        Pattern sample = Pattern.compile(Pattern.quote("C&A"), Pattern.CASE_INSENSITIVE);
        Matcher match = sample.matcher(text);
        boolean cea = (!match.find()) || isStringUpperCase(match.group());
        return cea;
    }

    private static boolean isStringUpperCase(String str) {
        char[] charArray = str.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            if (Character.isLetter(charArray[i])) {
                if (!Character.isUpperCase(charArray[i]))
                    return false;
            }
        }

        return true;
    }
}
