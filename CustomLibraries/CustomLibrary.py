import re


def is_uppercase(text, char_index=0):
    sample = 'C&A'
    match = re.search(sample, text, flags=re.IGNORECASE)
    cea = match.group().isupper() if match else True
    return cea
