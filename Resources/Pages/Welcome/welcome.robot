*** Settings ***
Resource       ../../main.resource



*** Variables ***
${next_btn}         id=br.com.cea.appb2c:id/img_btn_next
${skip_btn}         id=br.com.cea.appb2c:id/btn_skip
${logo_inc}         id=br.com.cea.appb2c:id/c_a_logo~
${main_text}        id=br.com.cea.appb2c:id/onboarding_?_title
${main_text_4}      xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]
${signup_btn}       id=br.com.cea.appb2c:id/btCreateAcount
${signin_btn}       id=br.com.cea.appb2c:id/btLogin
${access_banner}    id=br.com.cea.appb2c:id/access_banner_title

*** Keywords ***
the page "${i}" is fully loaded
    Wait Until Element Is Visible    ${skip_btn}    timeout=30    error=None
    ${i}    Convert To Integer    ${i}    base=None
    Set Test Variable    ${i}    ${i-1}

the access page is fully loaded
    Wait Until Element Is Visible    ${access_banner}    timeout=30    error=None

I Can See the Skip button
    Wait Until Element Is Visible    ${skip_btn}    timeout=30    error=None

I click in the right arrow
    Go To Next Page

I have to se the text "${text}"
    ${cur_locator}    Run Keyword If    ${i} != ${4}  Create Main Text Selector
    ...    ELSE    Set Variable    ${main_text_4}
    Wait Until Element Is Visible    ${cur_locator}    timeout=30    error=None
    Element Should Contain Text    ${cur_locator}    ${text}
    Set Test Variable    ${text}    ${text}

The text needs to use capital letters properly
    ${is_upper}    Is Uppercase    ${text}
    Run Keyword If    ${i} == 5 and "${is_upper}" == "False"    Fail    Text should have the 'C&A' word capitalyzed, but it hasn't: ${txt}
    ...    ELSE IF    "${is_upper}" == "False"    Go to the next page and fail test    ${text}
    ...    ELSE IF    ${i} == 5    Pass Execution    No problems found
    ...    ELSE       Go To Next Page

Skip Welcome Section
    Click Element    ${skip_btn}

I have to see the signup and signin buttons
    Wait Until Element Is Visible    ${signup_btn}
    Element Should Be Visible        ${signin_btn}


## Helpers
Go To Next Page
    Wait Until Element Is Visible    ${next_btn}    timeout=30    error=None
    Click Element    ${next_btn}

Go to the next page and fail test
    [Arguments]    ${txt}
    Go To Next Page
    Fail    Text should have the 'C&A' word capitalyzed, but it hasn't: ${txt}

Create Main Text Selector
    ${i}    Convert To String    ${i}
    ${txt_locator}    Replace String    ${main_text}    search_for=?    replace_with=${i}
    [Return]    ${txt_locator}